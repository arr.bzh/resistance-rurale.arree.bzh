import React, { useState } from "react";
import { Link } from "gatsby";
import facebook from "../img/social/facebook.svg";
import logo from "../img/logo.png";

const Navbar = () => {
  const [isActive, setIsActive] = useState(false)

  return (
    <nav
      className="navbar is-transparent"
      role="navigation"
      aria-label="main-navigation"
    >
      <div className="container">
        <div className="navbar-brand">
          <Link to="/" className="navbar-item" title="Arrée Résistance Rurale">
            <img src={logo} alt="ARR" style={{ width: '2.5rem' }} />
          </Link>
          {/* Hamburger menu */}
          <button
            className={`navbar-burger burger ${isActive && 'is-active'}`}
            aria-expanded={isActive}
            onClick={() => setIsActive(!isActive)}
          >
            <span />
            <span />
            <span />
          </button>
        </div>
        <ul
          id="navMenu"
          className={` navbar-start has-text-centered navbar-menu ${
            isActive && 'is-active'
          }`}
        >
          {/* TODO: inline override of padding is a result of refactoring
                to a ul for accessibilty purposes, would like to see a css
                re-write that makes this unneccesary.
             */}
          <li className="navbar-item" style={{ padding: "0px" }}>
            <Link className="navbar-item" to="/arr/">
              L'Association
            </Link>
          </li>
          <li className="navbar-item" style={{ padding: "0px" }}>
            <Link className="navbar-item" to="/eoliennes/">
              Les éoliennes
            </Link>
          </li>
          <li className="navbar-item" style={{ padding: "0px" }}>
            <Link className="navbar-item" to="/blog/">
              Nouvelles
            </Link>
          </li>
          <li className="navbar-item" style={{ padding: "0px" }}>
            <Link className="navbar-item" to="/contact/">
              Contact
            </Link>
          </li>
          <li className="navbar-end has-text-centered">
            <a
              target="_blank"
              rel="noopener noreferrer"
              className="navbar-item"
              title="NON aux centrales éoliennes dans les Monts d'Arrée"
              href="https://www.change.org/p/non-aux-centrales-%C3%A9oliennes-dans-les-monts-d-arr%C3%A9e"
            >
              Signer la pétition
            </a>
            <a
              target="_blank"
              rel="noopener noreferrer"
              className="navbar-item"
              title="Faire un don via HelloAsso"
              href="https://www.donnerenligne.fr/arree-resistance-rurale/faire-un-don"
            >
              Faire un don
            </a>
            <a
              className="navbar-item"
              href="https://www.facebook.com/arree.resistance.rurale"
              target="_blank"
              rel="noopener noreferrer"
            >
              <span className="icon">
                <img src={facebook} alt="Facebook" />
              </span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
