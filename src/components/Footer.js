import * as React from 'react'
import { Link } from 'gatsby'

import logo from '../img/logo.png'
import facebook from '../img/social/facebook.svg'
import gitlab from '../img/gitlab-icon.svg'

const Footer = () => {
  return (
    <footer className="footer has-background-black has-text-white-ter">
      <div className="content has-text-centered">
        <img src={logo} alt="ARR" style={{ width: '14em', height: '14em' }} />
      </div>
      <div className="content has-text-centered has-background-black has-text-white-ter">
        <div className="container has-background-black has-text-white-ter">
          <div style={{ maxWidth: '100vw' }} className="columns">
            <div className="column is-4">
              <section className="menu">
                <ul className="menu-list">
                  <li>
                    <Link to="/" className="navbar-item">
                      Accueil
                    </Link>
                  </li>
                  <li>
                    <Link className="navbar-item" to="/arr/">
                      L'Association
                    </Link>
                  </li>
                  <li>
                    <Link className="navbar-item" to="/eoliennes/">
                      Les éoliennes
                    </Link>
                  </li>
                  <li>
                    <a
                      className="navbar-item"
                      href="/admin/"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      Admin
                    </a>
                  </li>
                </ul>
              </section>
            </div>
            <div className="column is-4">
              <section className="menu">
                <ul className="menu-list">
                  <li>
                    <Link className="navbar-item" to="/blog/">
                      Dernière nouvelles
                    </Link>
                  </li>
                  <li>
                    <Link className="navbar-item" to="/contact/">
                      Nous contacter
                    </Link>
                  </li>
                  <li>
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      className="navbar-item"
                      title="NON aux centrales éoliennes dans les Monts d'Arrée"
                      href="https://www.change.org/p/non-aux-centrales-%C3%A9oliennes-dans-les-monts-d-arr%C3%A9e"
                    >
                      Signer la pétition
                    </a>
                  </li>
                  <li>
                    <a
                      target="_blank"
                      rel="noopener noreferrer"
                      className="navbar-item"
                      title="Faire un don via HelloAsso"
                      href="https://www.donnerenligne.fr/arree-resistance-rurale/faire-un-don"
                    >
                      Faire un don
                    </a>
                  </li>
                </ul>
              </section>
            </div>
            <div className="column is-4 social">
              <a
                title="facebook"
                href="https://www.facebook.com/Arr%C3%A9e-R%C3%A9sistance-Rurale-105663747608698/"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img
                  src={facebook}
                  alt="Facebook"
                  style={{ width: '1em', height: '1em' }}
                />
              </a>
              <a
                title="gitlab"
                href="https://gitlab.com/arr.bzh/resistance-rurale.arree.bzh"
                target="_blank"
                rel="noopener noreferrer"
              >
                <img
                  src={gitlab}
                  alt="Gitlab"
                  style={{ width: '1em', height: '1em' }}
                />
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer
