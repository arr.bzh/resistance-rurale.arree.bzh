import React from 'react'
import Layout from '../../components/Layout'

// eslint-disable-next-line
export default () => (
  <Layout>
    <section className="section">
      <div className="container">
        <div className="content">
          <h1>Merci !</h1>
          <p>
            Nous avons été notifié de votre message et prendrons contact avec
            vous rapidemment.
          </p>
        </div>
      </div>
    </section>
  </Layout>
)
