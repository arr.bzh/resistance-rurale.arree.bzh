---
templateKey: blog-post
title: REUNION EN MAIRIE LE 16 NOVEMBRE 2024
date: 2024-11-17T17:03:00.000Z
description: >+
  Les membres d'Arrée Résistance Rurale ont rencontré samedi 16 Novembre le
  maire Hubert Le Lann et une grande partie des élus.


  L'objet de cette réunion est la demande de construction d'un mat de mesure déposé en mairie cette semaine

featuredpost: true
featuredimage: /img/article-ouest-france-du-17-novembre-2024jpg.jpg
---
Voici un article de presse qui résume bien ce qui s'est passé :

![](/img/article-ouest-france-du-17-novembre-2024jpg.jpg)
