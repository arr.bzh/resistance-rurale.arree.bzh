---
templateKey: blog-post
title: La mairie de Berrien initie une association de soutien au projet éolien
date: 2019-12-03T22:00:00.000Z
description: >-
  Une association de soutien au projet éolien "citoyen" lancée par la mairie et
  des futurs actionnaires privés.
featuredpost: false
featuredimage: /img/reunion-public-3-décembre-2019.jpg
tags:
  - taranis
  - eskell ar menez
  - mairie
  - Éoliennes
  - Berrien
---
![réunion publique 3 décembre 2019](/img/reunion-public-3-décembre-2019.jpg)

Voici notre compte rendu de <a href="https://www.letelegramme.fr/finistere/berrien/eskell-ar-menez-une-association-publique-de-citoyen-sur-l-eolien-creee-05-12-2019-12450140.php" target="_blank" rel="noopener noreferrer">la réunion publique 3 décembre 2019 organisé par la mairie de Berrien</a>.

Le maire commence par un recadrage : il ne s'agit pas d'une réunion d'information mais d'une réunion ouverte aux personnes souhaitant constituer une association loi 1901 qui participerait à l'élaboration du projet de parc  éolien sur Berrien.

Deux personnes du réseau  Taranis étaient présentes pour présenter l'expérience de Béganne co-géré /co-financé par des particuliers en association.
La mairie a indiqué qu'elle souhait ensuite fonder une société-projet "tripartite" comprenant la commune, un développeur éolien et des actionnaires individuels.

Le maire, Paul Quéméner, et une conseillère municipale, Bernadette Lallouet, ont insisté sur le fait que tout restait à faire, que rien n'était décidé, que les statuts de l'association étaient à discuter. La mairie a cependant déjà préempté des terres (via promesses de bail emphytéotique) sur une vaste zone, situé à l'ouest du hameau de Quinoualc'h, suivant la D42 jusqu'à la route de Keraden. Les promesses de bail indiquerait une compensation financière pour les propriétaires des terres.

Le maire a précisé que la mairie avait été solicitée par une quinzaine de développeurs éoliens ces deux dernières années et que la mairie avait donc décidé de prendre les devants en lançant elle-même un projet éolien dans le but d'en garder une certaine maîtrise et d'éviter que des promoteurs éoliens ne démarchent les propriétaires directement.

L'objet de l'association, tel que proposé par la mairie est le suivant :
  - Informer sur les problématiques d'énergie, de modifications climatiques et les solutions que peuvent apporter les énergies renouvelables et les économies d'énergies.
  - Contribuer à la création de sites de production d'énergie renouvelable, notamment éolien, qui seront cogérés par des citoyens-actionnaires.

Plusieurs habitants, venus se renseigner sur le projet et l'association à créer, ont posé de nombreuse questions, sur l'éthique et le bien-fondé de l'éolien sur le territoire. Plusieurs arguments ont été avancés, notamment d'un point de vue éthique : l'éolien augmente les factures électriques pour enrichir les seuls actionnaires, ainsi que du point de vue climatique : l'électricité française étant déjà largement décarboné, l'éolien (nécessairement couplé aux centrale à gaz due à l’intermittence) n'ont aucun intérêt pour le climat. Leur impact sur le territoire (riverains, paysages, biodiversité dans un parc naturel et à proximité d'espaces naturels protégés...) a également été rapidement évoqué.

La mairie n'a pas voulu entrer dans un débat suscité par toutes ces questions et a indiqué qu'une réunion d'information, où un débat pourra avoir lieu, sera organisée en début d'année 2020.

Un participant a demandé si l'objet de l'association pouvait être changé afin d'être plus ouvert à la réflexion sur le sujet et donc de supprimer l'impératif de "contribuer à la création de site de production d'énergie renouvelable, notamment éolien". Refus net des organisateurs, qui ont ensuite convié toute personne en désaccord avec l'objet "proposé" à sortir de la salle.  Presque la moitié des participants sont alors sortis.

Beaucoup ont déploré le peu de communication qui a été faite sur ce projet ainsi que l'impossibilité de débattre dès à présent des questions essentielles pour les habitants et notamment pour les futurs riverains du parc éolien.

Mais une question se pose également : le projet éolien de la municipalité serait-il un "moindre mal" face à des projets concurrents menés unilatéralement par des promoteurs éoliens ne voyant la beauté des Monts d'Arrée qu'à travers le prisme de l'argent du vent ?
Ou bien est-ce que l'appel aux citoyens n'est au final qu'un leurre destiné à mieux faire passer la pilule et le résultat sera au final le même ?
L’État sacrifie-t-il encore une fois les ruraux qui n'ont quasiment aucune compensation face à la dégradation de leur cadre de vie et de notre environnement à tous, si ce n'est l'aumône de sociétés multi-nationales auxquelles les petites communes rurales seront de plus en plus dépendantes ?
