---
templateKey: blog-post
title: "Projet éolien à Scrignac : une présentation révélatrice des intentions
  du promoteur..."
date: 2025-03-04T11:32:00.000Z
description: >-
  INFORMATION 

  Ce diaporama du projet éolien de SCRIGNAC présenté par le promoteur Neoen nous donne un aperçu de ce qu'ils ont l'intention de faire et de l'incompatiblité de leur projet industriels avec les enjeux des monts d'Arrée !
featuredpost: true
featuredimage: /img/Monts-d-Arree.jpeg
---
<https://scrignac.bzh/wp-content/uploads/sites/129/2025/02/Presentation_Neoen_Comite_Projet_SCRIGNAC.pdf>

Attention c'est du lourd !

Bien entendu les impacts sont minimisés (exemple : "pistes à créer" ça veut dire de larges routes pour faire venir les grosses machines) ; avec 14 éoliennes le projet n'est pas acceptable mais il le devient en en enlevant une ?!.. Le doute est permis ! Et sans surprise, l'intermittence de la production est passée sous silence...

Finalement, les "retombées financières" (en dernière page) semblent la motivation véritable de ces projets - et il manque bien sûr les montants que le promoteur va empocher...

Voilà en tout cas un aperçu révélateur de ce qu'ils comptent faire des monts d'Arrée car des projets sont dans les tuyaux, dans la plupart des communes de la communauté de communes...

[](https://scrignac.bzh/wp-content/uploads/sites/129/2025/02/Presentation_Neoen_Comite_Projet_SCRIGNAC.pdf)
