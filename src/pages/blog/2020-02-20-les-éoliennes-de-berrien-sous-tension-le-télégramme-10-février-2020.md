---
templateKey: blog-post
title: '"Les éoliennes de Berrien sous tension"  Le Télégramme, 10 février 2020'
date: 2020-02-10T21:14:00.000Z
description: >-
  Le maire de Berrien, Paul Quéméner, ainsi que la vice-présidente du Parc
  Naturel Régional d'Armorique Gaëlle Vigouroux, ont exprimé leur volonté de
  déployer des usines d'aérogénérateurs dans les monts d'Arrée.
featuredpost: false
featuredimage: /img/banderolle-non-aux-éoliennes.jpg
tags:
  - PNRA
  - Paysages
  - Éoliennes
  - Berrien
---
L'article est consultable à cette adresse :

<https://www.letelegramme.fr/bretagne/les-eoliennes-de-berrien-sous-tension-11-02-2020-12500281.php>

Chacun jugera de la justesse des arguments avancés... et de l'adéquation de la position du PNRA avec [sa propre mission officielle](http://www.pnr-armorique.fr/Comprendre/La-charte-et-les-missions-du-Parc), à savoir :

> **protéger et valoriser le patrimoine naturel et culturel par une gestion adaptée des milieux naturels et des paysages.**

La charte en vigueur du parc ayant même comme fil conducteur 

> **Pour des paysages d’Armorique choisis.**

Choisis par qui ? On se le demande...
