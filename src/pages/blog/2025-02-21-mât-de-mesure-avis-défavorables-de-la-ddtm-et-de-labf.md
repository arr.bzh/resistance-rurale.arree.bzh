---
templateKey: blog-post
title: "Mât de mesure : avis défavorables de la DDTM et de l'ABF"
date: 2025-02-21T09:32:00.000Z
description: La DDTM a émis un avis défavorable à l'implantation d'un mât de
  mesure sur la commune de Berrien. L'Architecte des Bâtiments de France avait
  également émis un avis défavorable. Ces décisions confirment les arguments
  contre ce projet. L'association Arrée Résistance Rurale et les élu·es
  opposé·es à ce projet travaillent ensemble pour qu'il ne voie pas le jour.
featuredpost: true
featuredimage: /img/ouest-france-du-6-février-2025-2-.jpg
---
![](/img/ouest-france-du-6-février-2025-2-.jpg "Article Ouest France du 6 février 2025")
