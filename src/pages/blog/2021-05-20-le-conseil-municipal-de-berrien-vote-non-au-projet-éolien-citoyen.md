---
templateKey: blog-post
title: Le Conseil Municipal de Berrien vote NON au projet éolien "citoyen" !
date: 2021-05-20T12:16:42.925Z
description: >-
  Le 29 avril 2021, le conseil municipal a voté NON à la poursuite de
  l'implication de la Municipalité dans le projet éolien controversé d'éoliennes
  industrielles à Berrien.
featuredpost: false
featuredimage: /img/REVOLUTION_EOLIEN_SOLAIRE.jpg
tags:
  - Vote du Conseil Municipal
  - projet éolien à Berrien
  - projets dans les communes proches
---
Le 29 avril 2021, le conseil municipal a voté NON à la poursuite de l'implication de la Municipalité dans le projet éolien controversé d'éoliennes industrielles sur la commune. Le vote est clair : 10 voix contre, 3 voix pour et une abstention.

Pour rappel, ce projet avait été initié par quelques élus de la précédente équipe municipale.\
Devant la difficulté d'obtenir des informations sur le projet - qui était pourtant estampillé "citoyen", et consciente de l'ampleur des conséquences possibles d'un tel projet, l'association ARR avait demandé un référendum local sur la question, ainsi qu'un moratoire, le temps que la population puisse être correctement informée. Cela avait été accepté par l'équipe municipale.

Même si nous regrettons que le référendum ne puisse avoir lieu, nous saluons la décision courageuse de l'équipe municipale. Et nous remercions vivement les adhérents et sympathisants de l'association pour leur soutien !

Cependant, restons vigilants, car les promoteurs éoliens continuent de prospecter les environs et de démarcher les propriétaires des terres qui les intéressent. Des éoliennes industrielles sont en projets dans des communes proches : La Feuillée, Scrignac, Le Cloître-St-Thégonnec, Lannéanou et Plonevez-du-Faou près de Loqueffret - jusqu’où la liste va-t-elle s’allonger ?
