---
templateKey: blog-post
title: "Projet éolien à Berrien : l'arrêté d'opposition au mât de mesure est suspendu"
date: 2025-03-04T11:14:00.000Z
description: L'arrêté municipal d'opposition à la déclaration préalable de
  travaux pour la pose d'un mât de mesure à Keraden est suspendu.
featuredpost: true
featuredimage: /img/article-ouest-france-du-3-3-25.jpg
---
![](/img/article-ouest-france-du-3-3-25.jpg "Article Ouest-France du 03/03/2025")





Tout d'abord un grand merci aux personnes qui ont pu se déplacer pour assister au conseil municipal de jeudi dernier.

Le maire nous a fait la lecture de la décision de la cour administrative d'appel de Nantes.

Après cette lecture, des discussions se sont engagées entre le maire, les élus et les membres d'Arrée Résistance Rurale. Il en est ressorti que le maire accepte que les élus et les membres d'Arrée Résistance Rurale travaillent ensemble sur ce dossier. 

Voici en gros ce qu'il ressort de la décision de la cour administrative de Nantes :

WKN a saisi la chambre des référés pour aller plus vite. Le juge des référés se prononce sur la forme et pas sur le fond. Il a donné raison à WKN (le promoteur) et a ordonné la suspension de l'arrêté interdisant la pose du mât de mesure. La mairie a 10 jours pour annuler l'arrêté d'interdiction. Sur le fond, le dossier sera jugé mais cela prendra plus de temps.
