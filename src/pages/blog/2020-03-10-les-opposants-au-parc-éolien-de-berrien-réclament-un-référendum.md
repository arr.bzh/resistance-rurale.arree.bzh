---
templateKey: blog-post
title: Les opposants au parc éolien de Berrien réclament un référendum
date: 2020-03-10T17:01:01.760Z
description: >-
  Le Télégramme et Ouest-France, publie, en partie, notre communiqué en réaction
  aux propos tenus par Eskell ar Menez dans leurs pages. Merci à eux.
featuredpost: false
featuredimage: /img/tailles-eoliennes.png
tags:
  - eskell ar menez
  - Berrien
  - referendum
  - Éoliennes
---
L'association Eskell ar Menez s'était en effet exprimé le [4 mars 2020 dans le Télégramme](https://www.letelegramme.fr/finistere/berrien/eskell-ar-menez-et-le-projet-eolien-citoyen-04-03-2020-12517352.php) puis dans [Ouest-France](https://www.ouest-france.fr/bretagne/berrien-29690/berrien-les-habitants-s-impliquent-dans-leur-projet-de-parc-eolien-6768485) sans contradiction (ni réaction publique de la Mairie), donnant l'impression trompeuse qu'elle parlait au nom de la commune et des habitants de Berrien.

Ci-dessous notre communiqué dans son intégralité :

#### Le Télégramme du 4 mars 2020 nous donne des nouvelles du projet éolien dit "citoyen" à Berrien.

L'association Eskell ar Menez, initiée par la Mairie de Berrien et visant à rassembler des actionnaires pour ce projet, affirme qu'il vaut mieux "avoir des éoliennes citoyennes plutôt qu’un parc « industriel » privé". **Mais des éoliennes de 300 tonnes, de plus de 100 mètres de haut, coulées dans 1500 tonnes de béton armé chacune... restent industrielles, qu'elles soient estampillées avec un logo "citoyen" ou non !** À quand les kalachnikovs labellisées "commerce équitable" et donc inoffensives ?

![Comparaison de la tailles de "petites" éoliennes avec quelques monuments](/img/tailles-eoliennes.png)

Et surtout : les éoliennes sont inefficaces pour réduire notre dépendance aux énergies fossiles, qui représentent 70% de l'énergie consommée en France (très loin devant l'électricité) et sont les vrais responsables du dérèglement climatique.

Eskell ar Menez, de part ses statuts ayant pour but de "contribuer à la création de sites de production [...] éoliens", met la charrue avant les bœufs. Si le but est de contribuer à limiter le réchauffement climiatique alors s'imposer le choix des moyens, sans étude ni justification, est contre-productif : l'argent publique est limité. Si le but est de fournir des placements à 7% financés par les plus pauvres, alors il faut l'assumer, et pas tromper la population avec un logo "citoyen".

Car ce projet n'a rien de citoyen : la commune n'a pas les moyens financiers d'être seule majoritaire au sein d'une centrale électrique de plusieurs millions d'euros : **Il s'agit donc bien d'un projet privé**, la mairie ne sera pas décisionnaire.

Et même au sein de la société de projet, la seule ambition de la mairie et d'obtenir 50% des parts **avec les actionnaires privés d'Eskell ar Menez**, c'est à dire, en réalité, être minoritaire. En effet, **il n y a aucune raison que des actionnaires privés, qui par définition ne représente que leur propre intérêt, suivent les avis de la mairie et agissent dans l'intérêt général.**

Nous soutenons donc que "**le meilleur respect de la flore et de la faune, du paysage et du patrimoine des monts d’Arrée**" ainsi que "**l’intérêt de la population**"  c'est sans **AUCUN de ces aérogénérateurs industriels nocifs**.\
**Notre santé** et la **bio-diversité en danger**, **plusieurs milliards de subventions publiques par an**, les tonnes de **pales non recyclables**, le **CO2** de la centrale à gaz de Landivisiau (compagnon de l'intermittence de l'éolien), **des kilomètres de nouvelles lignes à haute tension** nécessaires pour distribuer les pics de production éoliens : **NON MERCI !**

Eskell ar Menez, cette association d'actionnaires, n'a aucune légitimité pour représenter les habitants de Berrien ! Or **ce projet, structurant pour la commune, a été lancé sans débat public.**

**Nous demandons** donc à la future équipe municipale **de déclarer un moratoire sur ce projet**, c'est à dire de **refuser toute étude financée par une société de projet éolien, jusqu'à l'organisation d'un referendum local au terme d'un réel débat démocratique**.

Soulignons que la **perte de dynamisme de la commune** est **déjà une réalité** : à Quinoualc'h, une famille a renoncé à son achat immobilier après avoir pris connaissance du projet éolien. C'est compréhensible : **qui risquerait sa santé en habitant volontairement à 500 mètres de ces machines infernales ?**

Rappelons enfin que les projets éoliens se multiplient dans les Monts d'Arrée et à proximité : La Feuillée, Scrignac, Le Cloître-St-Thegonnec, Lannéanou... Nous sommes cernés et **tous concernés**.

- - -

L'article du Télégramme reprenant notre communiqué : [les opposants au parc éolien de Berrien réclament un référendum](https://www.letelegramme.fr/finistere/berrien/les-opposants-au-parc-eolien-de-berrien-reclament-un-referendum-10-03-2020-12522257.php).

Notre réponse également reprise dans [Ouest-France](https://www.ouest-france.fr/bretagne/berrien-29690/berrien-arree-resistance-rurale-ne-veut-pas-d-eolien-6776015).
