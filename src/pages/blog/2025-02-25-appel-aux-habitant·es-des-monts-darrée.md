---
templateKey: blog-post
title: Appel aux habitant·es des Monts d'Arrée
date: 2025-02-25T21:25:00.000Z
description: |
  Recensement des projets éoliens dans les monts d'Arrée / le PNRA
featuredpost: true
featuredimage: /img/image-geobretagne-eolien.jpg
---
Nous constatons une pression de plus en plus grande sur les municipalités et sur les propriétaires de terrains, de la part de promoteurs, pour implanter des éoliennes industrielles dans les monts d'Arrée et dans le Parc Naturel Régional d'Armorique (PNRA). Bien entendu les nuisances sont passées sous silence par ces experts en

communication, à qui profiteront ces machines, aux dépens de la biodiversité locale (oiseaux, chauve-souris etc et leurs habitats) ; des paysages, des espaces naturels et du cadre de vie des habitant·es des monts d'Arrée et des personnes de passage.

Si comme nous vous voulez défendre les monts d'Arrée que vous aimez, aidez-nous à recenser les projets éoliens en cours - car l'opacité et la rétention d'informations semblent la norme sur ces projets - en interpelant vos élus sur les projets éoliens qui seraient prévus dans votre commune et si possible en essayant d'obtenir des informations précises (localisation, nombre et hauteur des machines, nom des sociétés...).

Des projets éoliens sont recensés à Berrien, Poullaouen, Le Cloître-Saint-Thégonnec, Scrignac, Huelgoat, Sizun, Carnoët, Pleyber-Christ (?)... ; si vous trouvez ou avez des informations sur ces projets ou d'autres dans les monts d'Arrée et en particulier dans le PNRA, merci d'envoyer un mail à l'association (resistance-rurale@arree.bzh) afin que nous puissions construire et partager une cartographie évolutive des projets.[](https://geobretagne.fr/mviewer/?config=/apps/eolien/config.xml)

N'hésitez pas à rejoindre l'association ARR ou d'autres associations locales ou à en créer de nouvelles et rassemblons-nous car chaque personne compte et c'est ensemble et en nombre que nous pourrons arrêter ces projets destructeurs et injustes.

Merci !

Au plaisir de nous rencontrer ![](https://geobretagne.fr/mviewer/?config=/apps/eolien/config.xml) 

PS : carte de l'éolien en Bretagne : <https://geobretagne.fr/mviewer/?config=/apps/eolien/config.xml>

PS - informons-nous sur la réalité de l'industrie éolienne sur le site de l'association nationale Vent de Colère : https://www.ventdecolere.org/

![](/img/image-geobretagne-eolien.jpg "L'éolien en Bretagne - carte geobretagne")
