---
templateKey: blog-post
title: Compte rendu de la réunion du 10 Juillet 2024 (Salle Asphodèle Berrien)
date: 2024-09-24T11:40:00.000Z
description: |-
  Réunion pour faire le point des actions, du projet.
  Tous les membres d'Arrée Résistance Rural, ainsi que les élus, étaient conviés
featuredpost: true
featuredimage: /img/banderolle-non-aux-éoliennes.jpg
---
Une réunion à l'initiative d' ARR, concernant le projet de parc éolien sur la commune de Berrien. a eu lieu le 10 juillet. Etait invités les adhérents de l'asso, l'équipe municipale et toute personne désireuse d'avoir des informations sur ce projet.

**Point à date sur le projet** :

➢ Démarchage, en Mars dernier par la société Tact, informant quelques habitants de la commune d'un projet éolien sur la commune. Et ce, à la demande du promoteur WKN (filiale française de WKN Allemagne)

➢ La zone ciblée est le sud de la D42 entre le bourg et Quinoualc’h sur les terrains déjà visés par le premier projet citoyen.

Monsieur le Maire, nous confirme avoir reçu le promoteur du projet la Ste WKN, et la Sté TACT, et qu'à ce jour, hormis l'emplacement, aucune information concrète et réelle ont été données. Il s'agissait d'une présentation générale d'un projet éolien, allant de 3 à 5 éoliennes, et pas spécifiquement de celui de Berrien. On peut donc supposer que le projet aurait au minima cette envergure. Un projet à plus petite échelle n’étant pas rentable.

Un des élus présents, semble en total désaccord avec le maire, pour lui WKN a été bien plus précis dans la présentation du projet. Il y aurait effectivement entre 3 et 5 éoliennes et leur taille évolueraient entre 170 m et 180 m. Et, la pose d'un mat de mesure est prévue au cours du 3eme trimestre 2024, ce que réfute le maire.

Mais, le maire s'engage à prévenir rapidement l'association si une demande de pose de mât lui parvient.

Les membres d' ARR demandent aux élus leur position par rapport à ce projet. Pour Hubert Le Lann la position de la mairie est la même qu'en 2021 lorsque le projet citoyen a été abandonné. Lors d'une réunion, le 4 juillet en mairie, le sujet a été abordé, et il semblerait que la majorité des élus soit contre (9 sur 13, 3 abstention et 1 favorable).

Les membres dARR demande à ce qu'une délibération soit votée lors du prochain conseil municipal, comme une majorité d’élus l'ont demandé (cf article du télégramme du 05/07/2024). Monsieur le Maire a pris note de notre demande, mais n’a pas pris d’engagement

**Réflexions sur l'éolien et par quoi le remplacer :**

Plusieurs personnes nous font remarquer que si nous voulons avoir plus de poids pour refuser le projet éolien, il faut peut-être réfléchir à proposer une solution de remplacement.

Il nous faut, bien évidemment, garder en tête la sobriété énergétique, et les idées ne manque pas pour tenter d'être moins consommateur et moins dépendant.

Il est évoqué la possibilité d'achat groupé de panneaux solaires

Un élu de La Feuillée pense que la recherche d'autonomie énergétique dans les communes et chez les particuliers est primordiale.

Mais, existe t-il un projet susceptible de remplacer le projet éolien dans notre commune ?

Le maire informe les participants qu'un projet de parc photovoltaïque sur le site des Kaolins a été initié par la commune. Le conseil municipal s'est réuni le 4 juillet pour valider le devis concernant l l'étude d’impact nécessaire à ce projet.

Ce sera un projet citoyen. Une réunion publique sera organisée en septembre et tous les habitants de la commune seront conviés.

In fine, c’est le préfet qui décide si oui ou non un projet éolien voit le jour sur son territoire. Mais une opposition de la mairie, des habitants et la proposition d'un parc photovoltaïque dans la commune peut influer sur sa décision.

**Suite des actions ARR :**

Même si le conseil municipal s'oppose au projet éolien, et malgré le projet photovoltaïque nous devons rester vigilant. WKN ne va pas abandonner son projet, il y a trop d'argent en jeu.

Comment faire obstacle aux promesses de bails ?

Les propriétaires des parcelles concernées par le projet sont connus, et certains en ont parlé à la mairie. ARR transmettra au maire des documents expliquant l'arnaque des baux emphytéotiques et fera un courrier aux propriétaires terriens. Ce courrier sera accompagné de documents sérieux expliquant les faces cachées d'un bail, et toutes les contraintes que le promoteur éolien garde sous silence. Quinze jours après l'envoi de ce courrier, ARR le fera paraître dans la presse.

**Tract** : Le tract sera revu (rajout du projet photovoltaïque et de la date de la réunion publique) , imprimé et distribué dans les boîtes aux lettres des habitants de Berrien. Dans la deuxième quinzaine d'Août par les membres d'ARR.

Une distribution pourra être faite dans les commerces, aux marchés du Huelgoat et de Quinoualc’h dès que les tracts seront prêts.

**Adhésion :**

Nous allons adhérer à Vent de Colère (40 €) et Environnement Durable (50 €)

Et, nous allons contacter des communes et des associations qui comme nous se battent (ou se sont battus) contre un projet éolien. Ainsi qu'Eric Ferec et Yann Queffélec.
