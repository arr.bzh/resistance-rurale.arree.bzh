---
templateKey: blog-post
title: Nouveau projet éolien privé sur la commune de BERRIEN
date: 2024-03-20T13:12:00Z
description: >-
featuredpost: true
featuredimage: /img/WKN/zone-implentation-WKN.jpeg
tags:
  - WKN
  - projet éolien à Berrien
---

Début mars 2024, une société de consulting nantaise (TACT) est venue démarcher les habitants de Berrien pour recueillir, par des questions biaisées, des informations vraisemblablement destinées à monter un dossier « d'acceptabilité sociale » pour leur projet. Bien sûr le questionnaire ne nous demande pas si nous voulons ou pas de leur projet.

![Panneau de protestation 3](/img/WKN/Panneau-3.jpeg)

Vous trouverez les 4 pages de ce questionnaire en bas de cette page.

 - [Article du Télégramme](https://www.letelegramme.fr/bretagne/a-berrien-un-nouveau-projet-eolien-reveille-la-colere-de-riverains-6557281.php) (également [en pdf](/img/WKN/%C3%80%20Berrien,%20un%20nouveau%20projet%20%C3%A9olien%20r%C3%A9veille%20la%20col%C3%A8re%20de%20riverains%20Le%20T%C3%A9l%C3%A9gramme.pdf))
 - Rejoignez la lutte, [adhérez à ARR](https://www.helloasso.com/associations/arree-resistance-rurale/adhesions/adhesion-annuel) (vous pouvez aussi [nous envoyer un petit message](/contact/)) !
 - [Signez la pétition !](https://www.change.org/p/non-aux-centrales-%C3%A9oliennes-dans-les-monts-d-arr%C3%A9e)
 - [Suivez-nous sur Facebook](https://www.facebook.com/arree.resistance.rurale).


Ce projet est porté par l'entreprise allemande WKN et vise la même zone qu'il y a 2 ans 1/2, située entre le bourg et Quinoualc'h.
WKN est une jeune société inconnue dans le secteur.

![Zone prévue d'implentation du parc éolien](/img/WKN/zone-implentation-WKN.jpeg)

Il y a sur cette zone :
- Une stèle et un tumulus
- Un marais
- Elle est située en bordure d'une ancienne ZNIEFF (Zone naturelle d'intérêt écologique faunistique et floristique)

Les diverses instances et institutions :

La mairie :
- Elle est sollicitée tous les ans par plusieurs sociétés de développement éolien
- Elle n'a aucun poids pour arrêter les projets
- Pour rappel, le Conseil Municipal s'était cependant prononcé il y a 2,5 ans contre un tel projet éolien sur la commune de Berrien, projet qui avait finalement été abandonné.

Le PNRA :
- Il centralise les [zones propices à l'installation des éoliennes](https://www.bretagne.developpement-durable.gouv.fr/les-zones-d-acceleration-des-energies-a5663.html) - avec la DREAL ?
- Il n'a aucun poids dans les décisions finales qui sont proposées par le préfet

À priori, si TACT mène une enquête auprès des habitants, c'est que les études ne sont pas forcément lancées.
![Panneau de protestation 1](/img/WKN/Panneau-1.jpeg)
**Exprimons notre opposition au projet au plus vite**, avant ce samedi 6 avril si possible (date attendue de rendu des questionnaires distribués par l'agence TACT), en écrivant à l'agence de consulting et / ou au développeur, en renvoyant le questionnaire distribué (disponible ci-dessous) ou en rédigeant un courrier (exemple ci-dessous).

[**Le questionnaire WKN au format PDF** (pour impression)](/img/WKN/questionnaire-projet-eolien-WKN.pdf)

[**Exemple de courrier** envoyé par une habitante de Berrien](/img/WKN/Lettre-pour-TACT-WKN.pdf)

![Page 1 du questionnaire TACT](/img/WKN/questionnaire-WKN-page1.jpeg)
![Page 2 du questionnaire TACT](/img/WKN/questionnaire-WKN-page2.jpeg)
![Page 3 du questionnaire TACT](/img/WKN/questionnaire-WKN-page3.jpeg)
![Page 4 du questionnaire TACT](/img/WKN/questionnaire-WKN-page4.jpeg)

- Coordonnées de l'agence de consulting :

Agence TACT<br>
8 rue Saint-Domingue<br>
44200 Nantes<br>
https://agencetact.fr/<br>
(la lecture de leur site internet est instructive...)

- Coordonnées du développeur :

WKN France<br>
Siège et Agence Grand Ouest<br>
Immeuble Le Sanitat<br>
10 rue Charles Brunellière<br>
44100 Nantes<br>
+33 (0)2 40 58 73 10<br>
contact(arobase)wkn-france.fr<br>
https://www.wkn-group.com/en/
