---
templateKey: blog-post
title: Si le "remède" est pire que le mal ?
date: 2020-10-30T16:11:24.467Z
description: >-
  Si le "remède" est pire que le mal, peut-être devrait-on le remettre en
  question ?

  Reporterre nous informe sur l'exploitation minière démesuré dernière nos
  énergie dites "verte" ou "propre".
featuredpost: false
featuredimage: /img/arton21529-be345.jpg
tags:
  - eoliennes mines
---
## [Les minerais : le très noir tableau des énergies vertes](https://m.reporterre.net/Les-minerais-noir-tableau-des-energies-vertes)

Extraits :

« L’exploitation minière influence potentiellement 50 millions de km² de la surface terrestre »

Pour comparaison,  c'est 17.000 fois la surface des zones d'exclusions de Tchernobyl + Fukushima.

« Les menaces pour la biodiversité augmenteront à mesure que les mines cibleront des matériaux pour la production d'EnR et, sans planification stratégique, ces nouvelles menaces pourraient surpasser celles évitées dans l'atténuation du changement climatique. » 

« En clair, aucune société n’est en mesure de contrôler et de connaître l’ensemble de la chaîne de production. » Ce serait le cas d’EDF et d’Engie, qui font appel à la société SRGE pour leur fournir certaines éoliennes offshore. Or celles-ci sont fabriquées avec des aimants nécessitant du néodyme, extrait – principalement en Chine – à l’aide d’un mélange à base d’uranium et de thorium déversés ensuite dans l’environnement. Pour chaque tonne de néodyme produite, entre 340.000 et 420.000 m³ de gaz toxiques seraient produits, ainsi que 2.600 m³ cubes d’eau acide et une tonne de déchets radioactifs. Pour autant, aucune des deux entreprises françaises ne mentionnent ces risques dans leur plan de vigilance. »

Oups...

Il ne s'agit pas de dire que les énergies renouvelables électrique sont à proscrire, mais leur développement, doit se faire dans une considération globale pour limiter leur impact :

Non aux déploiement des ENR qui servent a perpétuer une civilisation industrielle inégalitaire et mortifère !
