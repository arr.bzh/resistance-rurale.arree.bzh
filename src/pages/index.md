---
templateKey: index-page
title: ARRÉE RÉSISTANCE RURALE
image: /img/Monts-d-Arree.jpeg
heading: NON aux centrales éoliennes dans les Monts d'Arrée
subheading: NON aux centrales éoliennes dans les Monts d'Arrée
mainpitch:
  title: >-
    Les Monts d'Arrée sont actuellement la proie de promoteurs éoliens, et la
    commune de Berrien est une de leurs premières cibles.
  description: >-
    Seul un NON ferme de la population pourra empêcher ce projet !
testimonials:
  - title: Témoignages d'habitants qui subissent le voisinage des éoliennes industrielles
    author: Marc-Antoine Chavanis
    quote: >-
        La vocation première d'un parc naturel régional, c'est l'ancrage des gens au pays. Il est quand même absurde d'inciter les gens à fuir en leur mettant des éoliennes.
    video: https://www.youtube-nocookie.com/embed/Vf9EbpzDvoY?si=TyII4Kt682tbOMwj&amp;start=6195
---
![Panneau de protestation 2](/img/WKN/Panneau-2.jpeg)

### Protégeons les derniers refuges de la faune et de la flore !

Après un premier projet initié puis abandonné (en 2021) par la mairie de Berrien, **le site est de nouveau convoité par un promoteur**, un grand groupe industriel allemand [WKN Group](/blog/2024-03-20-Reunion-ARR-contre-le-projet-WKN/).

**Les éoliennes** seraient implantées entre **le Bourg**, **Quinoualc’h**, **Cozcastel**, **Keraden**, **Kernevez** et **Tredudon-le-Moine**. Un tel projet paraît insensé aux yeux des habitants qui aiment les Monts d’Arrée pour la beauté de ses paysages et pour la qualité de vie qu’on peut y trouver.

- Notre santé en danger
- [Perte de valeur de notre patrimoine](https://france3-regions.francetvinfo.fr/bretagne/finistere/quimper/le-promoteur-d-un-parc-eolien-condamne-a-indemniser-des-riverains-nos-maisons-sont-devenues-invendables-2947655.html) et d’attractivité de notre territoire
- Paysages défigurés
- Dévalorisation des biens immobiliers
- Perte de biodiversité et fuite du gibier
- Augmentation de nos factures d’énergie sans diminution des émissions de CO2
- Finies les nuits étoilées !

**Notre objectif : L’ABANDON DE TOUT PROJET DE PARC ÉOLIEN À BERRIEN** et tout comme nous nous opposons à tout projet éolien industriel dans les bocages encore préservés des Monts d’Arrée.

![Panneau de protestation 4](/img/WKN/Panneau-4.jpeg)
